### The Team
* Jackie Gragnola: Senior Manager, Marketing Campaigns & ABM
* Megan O'Dowd: Account Based Marketing Manager
* Christina McLeod: Account Based Marketing Manager

### [ABM Handbook >>](https://about.gitlab.com/handbook/marketing/account-based-marketing/)

### Quick Links
* Slack: [#abmteam](https://gitlab.slack.com/archives/CFBT2HSEB)
* [Create an Issue >>](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new)



