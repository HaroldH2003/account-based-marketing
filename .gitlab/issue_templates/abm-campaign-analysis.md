## Overview


### Links to Reports/Dashboards
* 

### Key screenshots
<!-- add screenshots here, use arrows/boxes to highlight key areas of focus -->


### Key findings
*What are the most important takeaways from analyzing the data?*

1. 
1. 
1. 

### Optimizations/next steps
*What can/should we action on as a result of these findings?*

1. 
1. 
1. 

### LinkedIn Benchmarks
- Sponsored Ad CTR: 4-0.56%
- InMail Open Rate: 30%

### Demandbase Benchmarks
_All benchmarks based on a 3-month period._ 
- Reached: 75% or greater
- Visited: 40% or greater
- Clicked: 25% or greater
- Lifted: 25-30% or greater
- CTR: 0.04-0.06%

##### Campaign metrics defined:

- Reached: Total # of accounts served at least 1 impression
- Visited: Total # of accounts that have been onsite during the campaign 
- Clicked: Total # of accounts that have clicked through an ad
- Lifted: Total # of accounts that have more engagement (page views) during the campaign  compared to the baseline 
- Engaged: Total # of accounts that have had 3 or more sessions within a 30 day period


<!-- PROJECT MANAGEMENT:

Note: For estimate, add expected time (20 minutes, 2 hours, 1 day).

/label ~demandbase ~"demandbase::reporting" ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"

-->
