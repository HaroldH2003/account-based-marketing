## Overview
This issue template is used for documenting kick-off call between ABM, Sales, FMM, SDR in the roll out of Tier 1 - <Insert Account Name> campaign.



## Action items
* [ ] Name this issue `Tier 1 kick-off call: <name of account>` (ex. Tier 1 kick-off: JP Morgan)
* [ ] Update milestone, due date, and assignee in project management section at bottom of issue
* [ ] Open Tier 1 epic (ex: [here](https://gitlab.com/groups/gitlab-com/marketing/-/epics/3027))
* [ ] Open Tier 1 issues (using Tier 1 [issue templates](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/tree/master/.gitlab/issue_templates)) and the below additional issues. Relate all to epic
     * [ ] [Campaign launch issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=abm-campaign-launch)
     * [ ] [ABM Design Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=abm-design)
* [ ] Schedule kick-off call meeting invite with the below team members. 
     * Required: SAL, BDR, Marketing Campaigns Manager, Field Marketing Manager
     * Mark as optional: ASM, BDR Manager, Manager: Marketing Campaigns, Director: Revenue Programs 
* [ ] Add a notes doc to kick-off call meeting[template here](https://docs.google.com/document/d/
* [ ] Create account folder in [ABM Google Drive](19SXVmUVPIfp8MzfdGxouw7GyM3nEAb4jSUeJoCcdMJw/edit?usp=sharing)
https://drive.google.com/drive/folders/1UpUrXi35MIUdndG1ZKdeiuN--wX_jPru?usp=sharing). You can move the notes doc and add all call recordings here. 
* [ ] Clone Tier 1 slides [template](https://docs.google.com/presentation/d/1eigOG77HWfxGSFz1xiWBez89fnkiheJulAmbbMoB4RA/edit?usp=sharing)
* [ ] Schedule marketing prep call with Marketing Campaigns Manager to review slides, issues and action items.

<!-- PROECT MANAGEMENT - update estimate and weight!

UPDATE:
/estimate 
/weight 1

For estimate, add expected time (20 minutes, 2 hours, 1 day). 
Weight guidelines for campaigns: https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#fundamentals 

STANDARD:
/label ~"ABM" ~"mktg-status::plan" ~"ABM-Priority::TBD"
/assign me
More on quick actions: https://docs.gitlab.com/ee/user/project/quick_actions.html


--> 
