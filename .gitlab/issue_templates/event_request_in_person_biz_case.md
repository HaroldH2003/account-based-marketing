Purpose of this issue template is for Account Based Marketing to share business case for in person events as we reenter from the COVID pandemic and have travel limitations in place. 

#### Please do not link out to separate gdocs. All information should be added directly in the issue. Given this issue will note travel details, it has been marked confidential. 

# Overview 
### Date 
### Location
### GitLab Attendees

List out specific people or number of people & their role - ex. 2 FMMs, 4 SAs, 2 SALs - all within driving distance of the venue.
Attendeed must receive approval to attend in order to submit expense reports. 

### How will attendees get to the event?
 I.e. 1 will drive 2 will fly.  

### Number of overall event attendees

### COVID specific guidelines
List or Link to the specific details

# Timeline to Contract
In this section, please note out any contract we already have in place or if we expect to receive a contract with a tight timeline to make a decision to attend or not attend. 

# Cost to participate 

# Expected ROI 
If we attended this event in the years past, please share the linear contribution to pipeline & link to the details. 
* Customers/prospects targeted to engage with: 
* Target SAO: 
* Estimated pipeline: 

## Next Steps 
1. Assign to your manager
1. Manager to review/provide feedback 
1. Manager will pull in VP of Integrated Mktg & CMO for next steps

/label ~"Account Based Marketing" ~"In person event::Requested" ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"
/confidential
