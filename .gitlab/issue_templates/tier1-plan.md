## Overview
This issue template is used for high-level tier 1 campaign plan based on kick-off call. It will include key topics to touch on, specific verbiage to use, asset ideas, etc. The next step will be to create a campaign launch issue that will explain each tactic in detail.

## Action items
* [ ] Name this issue Tier 1 Campaign Plan: <name of account> (ex. Tier 1 Campaign Plan: JP Morgan)
* [ ] Update milestone, due date, and assignees in project management section at bottom of issue

## ABM Campaign Overview

### Account Information
- SFDC Account
- SAL provided notes docs, chorus calls, etc
- Kick-off call doc
- Demandbase account

### ABM Initial Campaign Details
##### Tactics being leveraged for this account
Specific CTA links and ad examples to be added to campaign launch issue
* [ ] LinkedIn Promoted Ads  
* [ ] LinkedIn InMail
* [ ] Demandbase (Display Ads)
* [ ] BDR Outbound Play
* [ ] Custom Drift chatbot message and alerts for BDR
* [ ] Direct Mail
* [ ] Email Marketing

##### Resources/Assets
1.
1. 
1. 

### Metrics
_Monthly metric updates will be provided in campaign performance issue_

**SFDC Leads** 
- #of leads (YYYY.MM.DD) 
- filter logic/SFDC report (link)

**Engaged Leads/Contacts** The total number of engaged people in the past 3 months from that account. An engaged person is defined as someone 
- Pre-campaign: # (YYYY.MM.DD)

**Engagement Points (minutes)** tracks the level of engagement over a 3-month period an account is spending with us and associates a point called an engagement minute. *Worth noting, this does not represent a true minute. (info [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#engagement-minutes))
- Pre-campaign: # (YYYY.MM.DD)
- Standard: TOFU: 2.4 and BOFU 130.3

**SAOs**
- Pre-campaign: # (YYYY.MM.DD)

## Marketing Messaging
✏️ key insights: (ex: digitizing the bank, innovate faster, improve customer experience, etc.)

### Short term relevant content
-
-
-

### Long term relevant content
-
-
-




<!-- Please leave the labels below on this issue -->
/label ~"mktg-status::plan" ~"ABM campaign" ~"ABM" ~"ABM-Priority::TBD"
/confidential
