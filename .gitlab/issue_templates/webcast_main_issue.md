<!-- Naming convention: [Webcast Title] - [3-letter Month] [Webcast Date], [Year] --> 

## Webcast Details
### Team
- ABM Manager: 
- Presenter:
- Host:
- Q&A Support:
### Date and Name
* [ ] Official webcast name: `webcast title`
* [ ] Dry run date and time: `MM-DD-YYYY`
* [ ] Webcast date and time: `example - 10:00am PDT`

## Content and resources
* [ ] Landing Page (link)
* [ ] Landing Page and Invite Copy doc (link)
* [ ] Dry Run & Day of Agenda (link)
* [ ] Salesforce Campaign (link)
* [ ] Marketo Program (link)
* Campaign UTM - (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)
* Finance tag: (if offering swag etc.)

📪 Campaign Attendee Details
#### Who are we inviting
- Roles: (Marketo roles can be found [here](/handbook/marketing/marketing-operations/marketo/#demographic-scoring))
- Marketo target list (link)
- SDR target list (link)

#### How are we inviting?
* [ ] Sales-Nominated
* [ ] Marketing Outreach (Marketo invite)
* [ ] Demandbase display ads
* [ ] SDR Outreach sequence 

## Due Dates
| Item | Due Date | Status |
| ------ | ------ | ------ |
| Webcast Prep Issue |  |  | 
| Program Tracking Issue |  |  | 
| Target List Creation Issue |  |  | 
| Sales Nomination Issue |  |  | 
| Marketo LP Copy & Automation Issue |  |  | 
| Email Invitation Issue |  |  |
| Paid Social Issue |  |  |
| Demandbase Campaign Issue |  |  |
| Follow Up Email issue |  |  |
| Add to Nurture |  |  |
| List Clean & Upload |  |  |

### Webcast Preparation To Do List:
- [ ] Open all webcast issues (found [here](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/tree/master/.gitlab/issue_templates)) and link above in table.  
- [ ] **For Self Service Only** You need to create reminder emails for self-service webcasts. For buyer progression webcats, reminder emails will send automatically from Zoom via the Zoom/Marketo integration. Zoom sends a reminder the day before the webcast and an hour prior to the webcast.

### To Review/Worth noting
- Only send promotional emails Tuesday, Wednesday, or Thursday for optimal results.
- Post links to additional, related resources during the event.
- Include "contact us" information and a clear CTA at the end of the presentation.
- Video recording of webcast uploaded to YouTube within 24 hours as event occurred.
- Send the recording to all registrants, whether they attended or not within 48 hours post webcast.

/label ~"mktg-status::wip" ~"ABM Campaign"
