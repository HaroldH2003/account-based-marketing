## Overview / Purpose
* [ ] Channel Marketing Manager to provide any insights from Crossbeam on channel relationships with account
* [ ] Technology Alliances Marketing Manager to provide any insights on alliance partner connections for account

### Account Information
* [ ] SFDC Account Link


### Findings
#### Channel


#### Alliances


<!-- PROECT MANAGEMENT - update estimate and weight!

UPDATE:
/estimate 
/weight 1

For estimate, add expected time (20 minutes, 2 hours, 1 day). 
Weight guidelines for campaigns: https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#fundamentals 

STANDARD:
/label ~"ABM" ~"mktg-status::plan" ~"ABM-Priority::TBD"
/assign me
More on quick actions: https://docs.gitlab.com/ee/user/project/quick_actions.html


--> 
