## Overview
This issue template is used for documenting design needs for ABM campaigns

## Creative needed
1.
1.
1.
1.  

## Resources 
- [ ] Canva folder

### Demandbase Display Ad Sizes
- 728x90
- 300x250
- 160x600
- 300x600
- 970x250
- 320x50
- 300x50

<!-- Please leave the labels below on this issue -->
/label ~"mktg-status::plan" ~"ABM campaign" ~"ABM" ~"ABM-Priority::TBD"
/confidential
