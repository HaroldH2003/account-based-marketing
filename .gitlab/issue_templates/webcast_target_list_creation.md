<!-- Purpose of this issue: To create the target list in Marketo. -->
<!-- Note: this is to be used in interim of overall segmentations being built in Marketo. -->

:exclamation: Dependency: The [program tracking issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_program_tracking) must be complete before work can begin on this issue.

## Submitter Checklist
* [ ] Name this issue `Target List Creation: <tactic/campaign name>` (ex. Target List Creation: Security Workshop)
* [ ] Due date: Set due date (dependent on this submitter checklist being completed *at least 5 business days prior*) based on [workback timeline helper](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)
   - [ ] **Lifecycle Stage (Lead Status):** (Raw, Inquiry, Nurture, MQL, Accepted, Qualifying, Qualified)
   - [ ] **Sales Segment:** (Large, MM, SMB)
   - [ ] **Region:** (APAC, AMER, EMEA)
   - [ ] **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH):** 
   - [ ] **Account list:** 
   * [ ] **Key Persona:** (Practitioner, Manager, Executive)
   - [ ] **Activity filters (if necessary):** activity last 30 days
   - [ ] **Inclusions:** (if including records on previous campaigns, MUST include the name as appears on SFDC campaign, and campaign membership statuses to exclude)
   - [ ] **Exclusions:** (if excluding records on previous campaigns, MUST include the name as appears on SFDC campaign, and campaign membership statuses to exclude)
   - [ ] Change due date using SLA Workback
* [ ] If this section (Submitter Details) is fully complete when issue created, change ~"mktg-status::blocked" label to ~"mktg-status::triage"

## Reviewer Checklist
* [ ] Triage Manager: Confirm that required details were provided and issue is ready to be worked
* [ ] Create target list in program (comment as necessary)
* Close out this issue.



<!-- DO NOT UPDATE - LABELS
/label ~"ABM Campaign" ~"mktg-status::blocked"
/confidential
-->

