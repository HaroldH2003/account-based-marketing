<!-- Name this issue: ABM Milestone Plan YYYY-MM-DD -->
## Overview / Purpose
**To outline what we plan to accomplish in the next 2 week milestone.**

This is a 2-step approach to prioritization (and realistic commitments). Often when we look at all the work, it can be overwhelming, so prioritization is key to making sure we aren't maxing out, are aligning our actions to what needs to be done first and aligned to KRs, and avoid burnout.

Check below when exercise complete for the upcoming milestone:
* [ ] Megan
* [ ] Jackie

### :link: Relevant Links
* [BOARD: prioritization board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536730?label_name%5B%5D=ABM)
* [BOARD: Current milestone priorities board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536720?milestone_title=%23started&label_name\[\]=ABM)
* [LIST: Megan's issues in Marketing](https://gitlab.com/groups/gitlab-com/marketing/-/issues?assignee_username%5B%5D=megan_odowd&scope=all&sort=updated_desc&state=opened)
* [LIST: Jackie's issues in Marketing](https://gitlab.com/groups/gitlab-com/marketing/-/issues?assignee_username%5B%5D=jgragnola&scope=all&sort=updated_desc&state=opened)

### :point_up: Guiding Principles
- Consider the time you have available in a 2 week period, recognizing meeting commitments and planned PTO.
- Only add issues to account for 70% of your available work hours. Don't max out at 100% since there will likely be things that arise and need immediate attention throughout milestone.
- Ask yourself: considering realistic time this milestone, am I overcommitting? The first to drop from a milestone should be "Low" priority issues based on definitions.
- Discuss milestone with Jackie and if need help determining what to drop and how to communicate (potentially) to issue requestors.

## :star: Action Items
1. Make sure all issues you're planning to work have the labels: ~"ABM" ~"mktg-status::wip" ~"ABM-Priority::TBD" (or relevant status label)
1. Prioritize issues aligned to OKRs
1. Assess issues you've put in the current milestone and identify the top 2-3 that should be in ~"ABM-Priority::Top" 
   - Remember that if everything is a priority, nothing is a priority. Gauge what is aligned to driving pipeline/revenue, and items that are dependencies to projects driving pipeline/revenue.
1. Assess issues that should be in ~"ABM-Priority::High" (2-3)
1. Consider what issues should be dropped to Low (in comparison to other actions needed) OR dropped out of the milestone and tackled in the future. If you think you should tackle it in the next milestone, or a few milestones out, place it in the appropriate milestone.

### Code & shortcuts

**Command for comment to put in top priority:** `/label ~"ABM-Priority::Top"`

**Command for comment to put in high priority:** `/label ~"ABM-Priority::High"`

**Command to add status, priority, ABM label and Milestone:**

```
/label ~"mktg-status::wip" ~ABM ~"ABM-Priority::TBD" 
/mileston
(type `e` at the end of mileston and it will populate your milestone options to select)
```

What the code above will do:
1. Apply the labels ~"mktg-status::wip" to show that this is an issue currently committed to being worked
1. Apply the labels ~ABM ~"ABM-Priority::TBD" to bring it into our [prioritization board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536730?label_name%5B%5D=ABM), which we will discuss in our next 1:1
1. Apply the milestone for that 2-week period so that we can look at all issues committed to being complete in the [current milestone priorities board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536720?milestone_title=%23started&label_name\[\]=ABM), make sure they are sized/scoped to be manageable in a realistic time (if not, we break them down to be more manageable).



<!-- PROECT MANAGEMENT - update estimate and weight!

UPDATE:
/estimate 1 hour
/weight 1

STANDARD:
/label ~"ABM" ~"mktg-status::wip" ~"ABM-Priority::Medium"
/due 
/epic https://gitlab.com/groups/gitlab-com/-/epics/1672
/confidential
/assign @megan_odowd @jgragnola
More on quick actions: https://docs.gitlab.com/ee/user/project/quick_actions.html
--> 
