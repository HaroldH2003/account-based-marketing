## Kickoff Sales Nomination Process
* [ ] Name this issue kickoff `Sales Nomination Process, <campaign name>`
* [ ] Set due date according to [SLA workback timeline](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] Create messaging for sales nomination kickoff process. Ensure you include [the process](https://drive.google.com/drive/folders/1QNB3DXXWtnmMBvzeHPkT7cRmigJdIWq5) for how to nominate and when nominations will be closed.  
* [ ] Alert sales and SDR teams that the sales nomination process is open

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"ABM Campaign"
/confidential
-->
