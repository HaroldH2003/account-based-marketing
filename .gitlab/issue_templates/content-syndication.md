## :notepad_spiral: Campaign Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **Campaign Owner:** 
* **Campaign Type:** [**Content Syndication**](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event if applicable
* **Type of Content:** (Whitepaper, eBook, Report, Video, or General) 
* **Content Title:** 
* **Launch Date of Content:**  
* **End Date of Content:** (estimated end date of campaign)
* **Allocadia ID (needed if utilizing Coupa - US/Netherlands):**
* **Campaign Tag (needed if NOT utilizing Coupa):** 
* **Goal:** (Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.)
* **Budgeted Cost:** 
* **Budget Holder:** ABM
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH if specific):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## :vertical_traffic_light: User Journey
(ABM Manager to provide a description of the user journey or description of the program)  

## :white_check_mark: ABM Checklist 
* Add leads to nurture stream? - `YES/NO` [please specify nurture stream](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#gtm-motion-nurtures)  
  * If YES, select **ONE** buyer stage of leads based on tactic messaging:  
      - [ ] Awareness (top-funnel tactic)  
      - [ ] Consideration (mid-funnel tactic)   
      - [ ] Decision/Purchase (bottom-funnel tactic)
  * If YES, please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads over time):
      - [ ] Single Lead List
      - [ ] Multiple Lead Lists

## :calendar: Deadlines 
* [ ]

## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the ABM Manager is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate to the SDR issue to the epic. 

## :construction_site: Prepare
* [ ]  Update label from ~"mktg-status::plan" to ~"mktg-status::wip" once approved and Campaign Details and Checklist are completed. 

## :moneybag: Financial
* [ ]  Spend added to Allocadia activity plan
* [ ]  Procurement issue or Coupa requisition created by FMC and linked to this issue - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	  	


## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/label ~"Content Syndication" ~"Account Based Marketing" ~"mktg-status::plan" ~"ABM" ~"ABM-Priority::TBD"
