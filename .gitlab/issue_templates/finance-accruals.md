## Overview / Purpose
Provide accruals for Demandbase to @joezhou1 in the final week of the month.

## Results
- Demandbase: $
- LinkedIn: $

## Demandbase
* Download data from Demandbase > Advertising > Campaigns ([link](https://web.demandbase.com/o/t/cmp?status=all&audience=lifetime&pages=&startDate=2022-04-01&endDate=2022-05-01))
* Change dates to current month
* Select all campaigns
* Screenshot the top metrics and drop in this issue

<!-- If multiple teams working out of Demandbase
* Download data from Demandbase > Advertising > Campaigns ([link](https://web.demandbase.com/o/t/cmp?status=all&audience=lifetime&pages=&startDate=2022-04-01&endDate=2022-05-01))
* Import to new sheet in [FY22 & FY23 ABM Accruals doc](https://docs.google.com/spreadsheets/d/1SFADY3BRK3rylXXmSnnTZNrBVBTd9si8CL8fRgnu6hw/edit#gid=1515825096)
* Add `SPLIT` function to dissect Campaign Name by ")]|" and name first column of the split: `Department`
* Create pivot with the following:
   - Rows: Department
   - Columns: N/A
   - Values: Spend ($)
   - Filters: Spend ($) does not equal BLANK or 0
* Add up totals for ABM/ABS => ABM Department Total
* Add up totals for FM => Field Marketing Total -->

## LinkedIn
* Download data from LinkedIN Campaign Manager > Campaigns ([link](https://www.linkedin.com/campaignmanager/accounts/509054846/campaigns?campaignGroupIds=%5B620339406%5D&campaignIds=%5B181204576%2C181285446%2C181293306%2C181294146%2C181295756%2C181298256%2C181299126%2C181299576%2C181300016%2C181414056%2C181414316%2C181414536%2C182129436%2C182129766%2C182130476%2C182300626%2C182302406%2C181296766%2C182128916%2C182130006%2C182130146%2C182130656%5D))
* Change dates to current month
* Put in order by Spend column
* Select all campaigns
* Screenshot the top metrics and drop in this issue


## Relevant Links
* [FY22 and FY23 ABM Accruals doc](https://docs.google.com/spreadsheets/d/1SFADY3BRK3rylXXmSnnTZNrBVBTd9si8CL8fRgnu6hw/edit#gid=1850179225)

<!-- Please leave the label below on this issue -->
/label ~"mktg-status::wip" ~"ABM" ~"ABM-Priority::TBD"
/epic https://gitlab.com/groups/gitlab-com/marketing/-/epics/2569
/assign @jgragnola
