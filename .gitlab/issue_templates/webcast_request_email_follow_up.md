<!-- Purpose of this issue: To request a follow up email be sent via Marketo. -->

## Submitter Checklist
* [ ] Name this issue `Email Follow-Up: <tactic/campaign name>` (ex. Email Follow-Up: Security Workshop)
* [ ] Due date: Set due date based on [SLA workback timeline](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* Review [email best practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#email-marketing-best-practices)
* **Provide logistical details:**
   - [ ] Requested Send Date: 
   - [ ] Requested Send Time (a specific time & add a timezone - in 15 min increments): 
   - [ ] Recipient List: (link to marketo/salesforce if available)
   - [ ] Is this email [operational or non-operational](https://about.gitlab.com/handbook/marketing/marketing-operations/#email-communication-policy)? `Operational/Non-Operational`
* **Multiple follow up emails:** 
   - [ ] Do you want to send different follow up emails to different audiences? YES/NO 
   - [ ] If yes, confirm you have provided copy for the different emails in your copy doc and have specified audiences for each email send.
         * Please note out the status of the records you want to email: i.e. Attended, or Attended and No Show, etc. [Status options.](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#campaign-type--progression-status) 
* **Provide email setup details:**
   - [ ] From Name: `default: GitLab Team`
   - [ ] From Email: `default: info@gitlab.com`
   - [ ] Respond-to Email: `default: info@gitlab.com`
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable.
* [ ] Change due date to `2-days post webcast`
* [ ] Once this section is fully complete - including final and reviewed email copy - change ~"ABM-Verticurl::blocked" label to ~"ABM-Verticurl::wip" and assign Verticurl Project Manager.

## Marketo work to be completed 
* [ ] Add email to Email Marketing Calendar - include link to this issue 
* [ ] Create the email in Marketo
* [ ] Send test email to the requester and designated additional reviewer/approvers in the "Reviewers/Approvers" section of Submitter Checklist above
* [ ] When test email sent, change label to `~ABM-Verticurl::review`
* [ ] Issue requester (or reviewers) provide approval, or updates in issue comment
* [ ] Set the email to send on the agreed upon date
* [ ] Comment that the issue is scheduled for deployment (including date and time)
* [ ] Copy/Paste screenshot of email in top of issue above Submitter Checklist
* [ ] Confirm that the email deployed successfully in the comments of the issue
* Close out this issue.

/label ~"ABM-Verticurl::blocked"
/confidential
