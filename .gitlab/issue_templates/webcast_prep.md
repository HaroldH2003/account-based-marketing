## ABM < NAME > Webcast Prep

### GitLab Team
- **PM**:  
- **TM**: 
- **Host**: 

### About the Webcast
- **When**: < date and time >
- **Who** < what will the audience list look like? > 
     - Account list to be invited: < link account list >
     - Roles: 
     - How are we inviting: 
- **Flow**: < x minutes presenting, x minutes demo, x minutes Q&A >
- **Content**: < creating new or reusing? If reusing, link what we have > 
- **Topic/theme**: 
- **Items to be discussed**:

## :memo: Content 

| Presenter| Bio | Slide Deck/Video Link | Project Link (if any) |
| ------ | ------ | ------ | ------ |
|   |  |  | | |
|  |  |  | | |

## :hourglass_flowing_sand: Deliverables
     
| Item | DRI | Due Date | Status |
| ------ | ------ | ------ | ------ |
| Copy for Landing Page |   |  |  |
| Finalized Agenda |  |   |  |
| Polling Questions and Q&A | | | |
| Content Completed (Demos, Videos, Slides) |   |  |  |

## :page_facing_up: Working Documents/Templates
- Landing page copy:[CLONE THIS DOC AND LINK COPY](https://docs.google.com/document/d/10_sM6Tk5rz_yGaiWvnoZPyjM1117UAk7QbPDBwTKe_s/edit?usp=sharing) document to write copy and speaker bios for the landing page. 
- Polling questions and "canned" Q&A questions copy:[CLONE THIS DOC AND LINK COPY](https://docs.google.com/document/d/1_END4nX-D-GQ06gBzwfWVneWZMbhANTl_HK53A6luoY/edit?usp=sharing) document to write questions to be used during the webcast.

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"ABM Campaign"
/confidential
-->
