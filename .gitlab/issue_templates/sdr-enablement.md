## Key Information

### :question: If you have a question about a lead or this campaign
-  Campaign epic: [LINK HERE]
-  Enablement session recording:  [LINK HERE IF APPLICABLE]
-  Slack channel for questions: `#NAMEHERE`

## Campaign Overview & Lead Journey
- **Campaign overview:**
- **Campaign objective:**
- **Who are we targeting?**
- **Why are we targeting them?**
- **How are we targeting these leads?**
    - Tactics: 
- **What messaging should I be using as an SDR?**
- **Last Intersting Moment (LIM) associated with this campaign:**
- **SFDC Campaign Name:**

### Lead Journey
- Flow Chart to be added
- 

## Available Content
- (Link PathFactory track, webcasts etc.)
-

### Outreach sequence 
- [LINK HERE]

**Note: Please clone and make any regional adjustments needed (i.e. delivery schedule, time zone, language, etc.)**


## 💁 Target Personas

#### Level: 
- Persona title

#### Function:
-

/label ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"
