## :notepad_spiral: Campaign Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **Campaign Owner:** 
* **Campaign Type:** [**Direct Mail**](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event if applicable 
* **Direct Mail Campaign Name:** 
* **Allocadia ID (needed if utilizing Coupa - US/Netherlands):**
* **Campaign Tag (needed if NOT utilizing Coupa):** 
* **Goal:** (Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.)
* **Budgeted Costs (swag item, order fees, shipping, etc.):** 
* **Budget Holder:** ABM
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH if specific):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## :vertical_traffic_light: User Journey
(ABM to provide a description of the user journey or program details)

## :white_check_mark: ABM Checklist  
* Add leads to nurture stream? - `YES/NO` [please specify nurture stream](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#gtm-motion-nurtures)  
  * If YES, select **ONE** buyer stage of leads based on tactic messaging:  
      - [ ] Awareness (top-funnel tactic)  
      - [ ] Consideration (mid-funnel tactic)   
      - [ ] Decision/Purchase (bottom-funnel tactic)
* Will this event require sales-nominated workflow? - `YES/NO`
* Will you need a Marketo landing page set up? `YES/NO`
  * If YES, please specify the landing page requested go live date: `Date`
  * If YES, please specify if your LP requires the below (copy must be provided in the copy doc)
    - [ ] Thank you/confirmation page
    - [ ] Confirmation email
    - [ ] Add to calendar feature in confirmation email
* Please specify email send days/times/time zones:
  * [ ] Sales Nominated: `First Send Date/Time/Time Zone`
  * [ ] Invite 1: `Day/Time/Time Zone`
  * [ ] Invite 2: `Day/Time/Time Zone`
  * [ ] Reminder: `Day/Time/Time Zone`
  * [ ] Follow up: `Day/Time/Time Zone`
    * Do you want to send different follow up emails to different audiences? `YES/NO` (If YES, you must provide copy for the different emails in your copy doc and have specified audiences for each email send)
* If sending invites, will you be utilizing a [DB1 list](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#pushing-demandbase-lists-to-marketo) or require CM support for a target list (15 business day SLA)? `DB1/CM Target List`

## :calendar: Deadlines 
* [ ]  

## :package: Swag
List out swag details here!

## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the ABM Manager is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate to the SDR issue to the epic. 

## :construction_site: Prepare
* [ ] Direct Mail lead list: Please link lead list here. 
* [ ] Update label from ~"mktg-status::plan" to ~"mktg-status::wip" once approved and Campaign Details and Checklist are completed. 

## :moneybag: Financial
* [ ]  Spend added to Allocadia activity plan
* [ ]  Procurement issue or Coupa requisition created by FMC and linked to this issue - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	

## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/label ~"Direct Mail" ~"Account Based Marketing" ~"mktg-status::plan" ~"ABM" ~"ABM-Priority::TBD"
