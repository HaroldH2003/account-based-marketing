<!-- Purpose of this issue: To request leads from a tactic to be added to an existing nurture program in Marketo. As the campaigns team builds out the automation logic and nurture programs in Marketo, these issues will likely no longer be needed as leads will automatically be added to the appropriate nurture programs. -->

## Submitter Checklist
* [ ] Name this issue `Add to Nurture: <Program Name>` (ex. Add to Nurture: Commit User Conference)
* [ ] Due date: Set due date based on [SLA workback timeline](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] Desired nurture program: [select from list here](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#active-nurture-programs)
* [ ] Buyer stage of leads based on tactic messaging (select one)
  - [ ] Awareness (top-funnel tactic)
  - [ ] Consideration (mid-funnel tactic)
  - [ ] Decision/Purchase (bottom-funnel tactic)
  - [ ] ~~Dynamically based on the records score~~ This is future state! 
* [ ] Please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads from a 3rd party vendor over time):
  * [ ] Single Lead List
  * [ ] Multiple Lead Lists
* [ ] Leads have been uploaded (confirm each time for multiple lead list uploads) 
  * **NOTE:** Leads must be uploaded before this issue can be triaged. If you have multiple lead lists, one add to nurture issue can be utilized for them all. Keep this issue in ~"ABM-Verticurl::blocked" status while waiting on list uploads.
* [ ] Once this section is fully complete, change ~"ABM-Verticurl::blocked" label to ~"ABM-Verticurl::wip"

## Marketo work to be completed
* [ ] Follow [instructions in Handbook](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#add-to-nurture-in-program)
* [ ] Comment to confirm leads have been added
* Close out this issue.

/label ~"ABM-Verticurl::blocked" 
/confidential
