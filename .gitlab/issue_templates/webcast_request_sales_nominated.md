<!-- Purpose of this issue: To request a sales-nominated email to be created via Marketo and set up for regular sends. -->

## Submitter Checklist
* [ ] Name this issue `Sales Nominated Email Invite Launch: campaign name` (ex. Sales Nominated Invite: Security Workshop)
* [ ] Due date: Set due date as date of first send based on [SLA workback timeline](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* **Provide logistical details:**
   * [ ] Requested First Send Date: 
   * [ ] Requested Daily Send Time: 
   * [ ] Requested Cadence: `default = M-F`
   * [x] Is this email [operational or non-operational](https://about.gitlab.com/handbook/marketing/marketing-operations/#email-communication-policy)? Sales Nominated Emails MUST be `Non-Operational`
* **Provide email setup details:**
   * [ ] From Name: `default: GitLab Team`
   * [ ] From Email: `default: info@gitlab.com`
   * [ ] Respond-to Email: `default: info@gitlab.com`
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable.
* [ ] Change due date to `-23 business days pre webcast` [(SLA workback timeline for assistance)](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] Once this section is fully complete, change ~"ABM-Verticurl::blocked" label to ~"ABM-Verticurl::wip" and assign Verticurl Project Manager.

## Verticurl Project Manager Checklist
* [ ] Create the email in Marketo (or review if already pre-set template)
* [ ] Send test email to the requester 
* [ ] When test email sent, change label to ~"ABM-Verticurl::review"
* [ ] Issue requester (or reviewers) provide approval, or updates in issue comment
* [ ] Schedule recurring email - [Handbook Instructions](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#sales-nominated)
* [ ] Comment to confirm that the email is scheduled to recur
* Close out this issue.

/label ~"ABM-Verticurl::blocked"
/confidential
