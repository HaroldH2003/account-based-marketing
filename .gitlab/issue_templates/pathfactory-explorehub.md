## Steps

* [ ] Upload new content
* [ ] Create new track
* [ ] Add new URL to existing recommended track
* [ ] Add form strategy to track
* [ ] Adjust form strategy on existing track
- [ ] create appearance

### Upload New Content
<details>
<summary>Expand for required details and action items</summary>

#### Required Details
* **Content Upload Type (choose one):** `URL` OR `File upload (up to 100MB)` OR URL`
* **URL Slug:** `be descriptive of the content with no stop words such as and/the/etc. (ex. gartnermqaro`)
* **Content Title:** 
* **Content Description:** `Add clear and concise description (2-3 sentences)`
* **Content Type:** [selection ONE from options here](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#content-types)
* **Content Language:** [include all relevant from options here](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#content-topics)
* **Expiry Date (if there is one):** `YYYY-MM-DD`

#### Action Items
* [ ] Upload content - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#how-to-upload-content)
* [ ] Add to [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
</details>

### Create New Track
<details>
<summary>Expand for required details and action items</summary>

#### Required Details
* **Track Type (choose one):** `Target Track` OR `Recommended Track`
* **Campaign/Tactic:** 
* **Custom URL Slug:** (use [format from Handbook]()) - to b added
* **Form Strategy:** 
* **Content (in order) for track:**
  - asset name as appears in Pathfactory
  - asset name as appears in Pathfactory
  - asset name as appears in Pathfactory
  - asset name as appears in Pathfactory

#### Action Items
* [ ] Create track - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#create-a-content-track)
* [ ] Add to [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
</details> 

#### Action Items
* [ ] Create listening campaign - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#set-up-a-new-listening-campaign)
* [ ] Add to [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
</details>

### Add new URL to exisiting recommended track
<details>
<summary>Expand for required details and action items</summary>

#### Required Details
* **New URL:** 
* **Link to recommended track:** 
* **Requested location (needs approval from MPM):** 

#### Action Items
* [ ] Add new URL to exisitng recommended track
* [ ] Add to [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
</details>

### Add form strategy to track
<details>
<summary>Expand for required details and action items</summary>

#### Required Details
* **Link to track:**
* **Requested form strategy:** 

#### Action Items
* [ ] Add form strategy to track
* [ ] Add to [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
</details>

### Adjust form strategy on existing track
<details>
<summary>Expand for required details and action items</summary>

#### Required Details
* **Link to track:**
* **Requested change:** 

#### Action Items
* [ ] Update form strategy on track
* [ ] Add to [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - [handbook process](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
</details>

<!-- UPDATE - PROECT MANAGEMENT DETAILS --> 

/epic 
/due 
<!-- PROJECT MANAGEMENT LABELS -->
/label ~"ABM campaign" ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"
