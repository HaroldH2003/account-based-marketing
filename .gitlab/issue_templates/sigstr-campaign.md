## ABM Terminus Campaign - [NAME]

This issue is to track the creation and provide the process for creating a Sigstr/Terminus campaign for ABM.

### To Do:
- [ ] Ensure SDR team and leadership is aware of launch
- [ ] Create banner and launch
- [ ] Link to creative: 

### Process: 
1. Create a CSV of accounts you want to target. Account lists require a header with either 'website', 'url', or 'domain' in order to import correctly. 
1. Create the banner you would like shown in SDR emails in Canva. Size: 900 wide x 240 pixels tall.
1. Login to Terminus and click the list icon at the left hand side.
1. Select `import list`
1. To target specific accounts, select CSV Upload. 
1. Drop in your CSV and import. 
1. Click campaigns icon at the left hand side - the flag icon.
1. In the top right-hand corner, select `create campaign`.
1. Typically, for ABM campaigns we use Targeted Campaigns in Terminus. Select Targeted Campaign and add campaign name at the bottom. 
1. Select your CSV list.
1. Add your banner image and the clickthrough URL (where you want clicking the ad to take a prospect).
1. Add start and end dates and launch.

### Resources
- [Terminus Handbook Page](https://about.gitlab.com/handbook/marketing/marketing-operations/terminus-email-experiences/)
- [ABM Tactics, Terminus](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/abm-campaign-tactics/#terminus-singnature-banner)


/label ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"