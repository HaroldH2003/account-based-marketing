## ABM Webcast Dry Run Prep

### Host will:
* [ ] Review role assignment
* [ ] Review communication best practices - chat and slack
* [ ] Review workshop slides
* [ ] Review housekeeping tasks
* [ ] Review flow of events
* [ ] Review polling uestions

### Housekeeping Tasks
* [ ] All presenter's to test ability to present
* [ ] Recap on presenting best practices
     * Presenters and moderator should have their video on. 
     * Presenters will present from their own computers
     * It is recommended the GitLab team uses one of the following [virtual backgrounds](https://drive.google.com/drive/u/0/folders/1LC60VGmiBkrCURa7q50aduFgaLB2SAVu)
* [ ] Use the slack channel for internal communication: ADD CHANNEL NAME HERE
* [ ] Those assisting with questions in the chat, use the slack channel if you are unsure how to answer

### Script and Timeline
 
#### Start of webcast
##### Moderator
 
Hello Everyone! Thank you for joining, we will be starting (5 Minutes).
* Share agenda slide
 
#### 5 minute mark
##### Moderator
 
Hello and welcome everyone!  Thank you for joining us today, we are very excited to have you here with us at this GitLab INSERT EVENT NAME Webcast!
 
My name is INSERT NAME, MODERATOR and I’m INSERT BIO. I’m excited to host this webcast for you all today.
 
Before we get started, I’m going to cover a couple of housekeeping items. 
  
Our agenda for today is up on your screen now.
 
Optional: (We’re going to have presentations followed by a demo for you to do while we are walking you through it.)
 
Feel free to ask questions throughout the presentation using the chat functionality. We will also save time at the end for questions. 
 
We will send the on-demand webcast recording as well as the presentation slides to you via email within the next day.
 
Optional: Announce gift/etc. "Also, as (FMM TO CONFIRM WHAT BONUSES IF ANY ARE OFFERED) a bonus today we will be drawing random names and giving away three prizes for people who submit the post-event evaluation.  To be considered, you’ll need to complete the post-event evaluation and provide the code word we provide at the end of the session. We’ll give you that code word and the link to the evaluation after today’s presentations are complete. The prizes are: List prizes you have agreed upon. So it’s worth filling out the survey!"
 
Optional: To get to know our audience a little better, we're going to launch a couple of polls throughout the workshop. We’re going to kick off our first poll now! (wait for polling to push - allow to run for about 10 seconds, then give 10-second warning that the poll is closing, comment on results)

** [FMM Dry Run Template for reference]([https://docs.google.com/document/d/1DzNkJnwlVOB1vw4yvgXtIxtddfw_x4u7WC9xAqSVEHM/edit?ts=5f6162ea#)


<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"ABM Campaign"
/confidential
-->



