## Overview
This issue template is used for account research and slide prep completed by the ABM Manager and the Marketing Campaigns manager prior to an ABM Tier 1 kick-off call. 

## Account Overview

## Resources
- Tier 1 account slide deck: (link here)

## Action items
* [ ] Name this issue Tier 1 Research: <name of account> (ex. Tier 1 Research: JP Morgan)
* [ ] Update milestone, due date, and assignees in project management section at bottom of issue
* [ ] Marketing Campaigns Manager to complete Marketing Campaign Account Analysis slide  
* [ ] ABM Manager to complete remaining slides
* [ ] Channel Marketing Manager to provide any insights from Crossbeam on channel relationships with account
* [ ] Technology Alliances Marketing Manager to provide any insights on alliance partner connections for account

## Demandbase Findings
- Journey stage:
- HIGH Intent keywords: 
- Top visited pages:
- Top engaged people: 

## Campaigns Findings
- How many people from the account have interacted with integrated campaigns
     - What are their titles/personas
- What integrated campaigns did they interact with, any particular asset/topic
- What are the contacts initial sources
- How do we make sure these contacts are included in relevant marketing activities (e.g. workshops, intelligent nurture, other FM events, Linkedin/Demandbase targeting)
- How many times have the contacts responded to our marketing efforts
- What stage of the funnel are the contacts in (INQ, MQL, SAO)
- Cohort analysis of similar accounts that are closed won - what channels and offers are successful in closing them

## Channel / Alliances Findings





<!-- PROECT MANAGEMENT - update estimate and weight!

UPDATE:
/estimate 
/weight 1

For estimate, add expected time (20 minutes, 2 hours, 1 day). 
Weight guidelines for campaigns: https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#fundamentals 

STANDARD:
/label ~"ABM" ~"mktg-status::plan" ~"ABM-Priority::TBD"
/assign me
More on quick actions: https://docs.gitlab.com/ee/user/project/quick_actions.html


--> 
