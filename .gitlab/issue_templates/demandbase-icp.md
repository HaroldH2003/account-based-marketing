Please note that this process (and issue template) may be deprecated or changed in the future for more scalable approaches to ICP. With questions, please reach out to `@jgragnola` in the #abmteam Slack channel.

#### Issue to propose a change ot our ideal customer profile outlined in the [handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/ideal-customer-profile/).

### Which ICP would you like to propose a change to?
- [ ] Large
- [ ] Public Sector
- [ ] Mid-Market 500+
- [ ] Mid-Market <500

### Please write a brief summary of the change you are proposing (including the WHY)

/label ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"
