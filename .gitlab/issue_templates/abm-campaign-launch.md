<!-- Purpose: for use by ABM team to detail channels, account list and campaign details. -->
## Campaign Overview
<!-- Summary of the overal campaign and goals  -->

## Channels
- [ ] Demandbase display ads
- [ ] LinkedIn banner ads
- [ ] LinkedIn InMail  

## Demandbase Campaign Details
### Overview
<!-- Summary of the campaign, target audience and goals  -->
- [ ] Budget
- [ ] Demandbase account list: 
- [ ] Display ads:
- [ ] Reach test: 
- [ ] Landing page w/UTM: 
- [ ] Flight dates: 
- [ ] UTM SFDC Report for tracking:
- [ ] Added to [Marketing Calendar](https://docs.google.com/spreadsheets/d/1ni6gKeWhjtrNppMdYvPESsCRjDbfVdYjTNtUtcNBFGg/edit#gid=571560493)

## LinkedIn Campaign Details
### Overview
<!-- Summary of the campaign, target audience and goals  -->
- [ ] Budget
- [ ] Account list: (export link or DB link if being executed via DB orchestration) 
- [ ] Display ads:
- [ ] Landing page w/UTM: 
- [ ] Flight dates: 
- [ ] UTM SFDC Report for tracking:
- [ ] Added to [Marketing Calendar](https://docs.google.com/spreadsheets/d/1ni6gKeWhjtrNppMdYvPESsCRjDbfVdYjTNtUtcNBFGg/edit#gid=571560493)




<!-- PROECT MANAGEMENT - DO NOT EDIT
/estimate 1 hour
/weight 1
/label ~demandbase ~"demandbase::campaign" ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"
/assign @megan_odowd 
More on quick actions: https://docs.gitlab.com/ee/user/project/quick_actions.html
-->
