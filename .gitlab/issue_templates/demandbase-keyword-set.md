## Keyword Set Overview
A Demandbase keyword set is a set of words that represent the things that prospective GitLab customers would be interested in reading about. Keyword sets can be general or more granular depending on the campaign. For example, our general GitLab keyword set includes Git, DevOps software, and CI/CD --  terms that people would be reading about if they were interested in buying GitLab. More information can be found [here](https://support.demandbase.com/hc/en-us/articles/360056755791-Understanding-Intent). 

## Keyword Set Goal & Topic
`Add here`

## To Do
* [ ] Work with aligned PMM/TMM to determine best pages to use for Demandbase to pull out and suggest keywords based on the desired topic for your keyword set
* [ ] (If applicable/available) Work with Digital Marketing to obtain a report of all the queries that people typed into Google and then clicked on one of our campaign ads (based on the desired topic for your keyword set).
* [ ] Start compiling your keyword set in Demandbase using the above data and then leverage the Demandbase "suggested keywords" feature which will be based on the keywords you have already added.
* [ ] Export and add information to [this](https://docs.google.com/spreadsheets/d/1VSN0mgPu_t6iwP_vXa4WmxX7CXtxGmtddlYVOChfIQk/edit?usp=sharing) sheet. Review with aligned PMM/TMM and Niall. Keyword set should have 20-100 keywords

## Relevant Links
* [How to create a keyword set](https://support.demandbase.com/hc/en-us/articles/360056755831-Set-Up-Demandbase-One-Intent-Create-Keyword-Sets)
* []()

<!-- PROECT MANAGEMENT - DO NOT UPDATE

/estimate 3 hours
/weight 1
/label ~"ABM" ~"mktg-status::wip" ~"ABM-Priority::TBD" ~demandbase ~"demandbase::platform"
/assign me
More on quick actions: https://docs.gitlab.com/ee/user/project/quick_actions.html
-->
