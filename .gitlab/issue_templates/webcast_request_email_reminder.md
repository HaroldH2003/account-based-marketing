<!-- Purpose of this issue: To request a reminder email be sent via Marketo. -->

## Submitter Checklist
* [ ] Name this issue `Email Reminder: <tactic/campaign name>` (ex. Email Reminder: Security Workshop)
* [ ] Due date: Set due date based on [workback timeline helper](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* **Provide logistical details:**
   - [ ] Requested Send Date #1: 
   - [ ] Requested Send Date #2:
   - [ ] Requested Send Time (a specific time & add a timezone - in 15 min increments): 
   - [ ] Recipient List: `default: Status To = Registered`
   - [ ] Reviewers/Approvers (email addresses must be provided)
* **Provide email setup details:**
   - [ ] From Name: `default: GitLab Team`
   - [ ] From Email: `default: info@gitlab.com`
   - [ ] Respond-to Email: `default: info@gitlab.com`
   - [ ] Is this email [operational or non-operational](https://about.gitlab.com/handbook/marketing/marketing-operations/#email-communication-policy)? `Operational/Non-Operational`
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable and shared with our Verticurl team
* [ ] If this section (Submitter Details) is fully complete when issue created, change ~"ABM-Verticurl::blocked" label to ~"ABM-Verticurl::wip"

## Marketo work to be completed
* [ ] Add email to Email Marketing Calendar - include link to this issue 
* [ ] Create the email in Marketo
* [ ] Send test email to the requester and designated additional reviewer/approvers in the "Reviewers/Approvers" section of Submitter Checklist above
* [ ] When test email sent, change label to `~ABM-Verticurl::review`
* [ ] Issue requester (or reviewers) provide approval, or updates in issue comment
* [ ] Set the email to send on the agreed upon date
* [ ] Comment that the issue is scheduled for deployment (including date and time)
* [ ] Copy/Paste screenshot of email in top of issue above Submitter Checklist
* [ ] Confirm that the email deployed successfully in the comments of the issue
* Close out this issue.

/label ~"ABM-Verticurl::blocked"
