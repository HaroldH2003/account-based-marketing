<!-- Purpose of this issue: To request an invitation email be sent via Marketo. Please note that for multiple invitations, multiple issues must be created in order to be addressed appropriately in time-based milestones. -->

## Submitter Checklist
* [ ] Name this issue `Email Invitation #X: Campaign name` (ex. Email Invitation 1: Security Workshop)
* [ ] Due date: Set due date based on [SLA workback timeline](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] NOTE that **if multiple invitations are needed**, multiple issues must be created
* Review [email best practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#email-marketing-best-practices)
* **Provide logistical details:**
   - [ ] Requested Send Date: 
   - [ ] Requested Send Time (a specific time & add a timezone - in 15 min increments): 
   - [ ] Recipient List: Please link to your completed [Target List](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_target_list_creation)
   - [ ] Is this email [operational or non-operational](https://about.gitlab.com/handbook/marketing/marketing-operations/#email-communication-policy)? `Operational/Non-Operational`
* **Provide email setup details:**
   - [ ] From Name: `default: GitLab Team`
   - [ ] From Email: `default: info@gitlab.com`
   - [ ] Respond-to Email: `default: info@gitlab.com`
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable.
* [ ] Change due date to email invite send date (Note: different date for email 1 and 2) found [here](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing). 
* [ ] Once this section is fully complete, change ~"ABM-Verticurl::blocked" label to ~"ABM-Verticurl::wip" and assign Verticurl Project Manager.

## Marketo work to be completed 
* [ ] Add email to Email Marketing Calendar - include link to this issue 
* [ ] Create the email in Marketo
* [ ] Send test email to the requester
* [ ] When test email sent, change label to ~"ABM-Verticurl::review"
* [ ] Issue requester provide approval, or updates in issue comment
* [ ] Set the email to send on the agreed upon date
* [ ] Comment that the issue is scheduled for deployment (including date and time)
* [ ] Copy/Paste screenshot of email in top of issue above Submitter Checklist
* [ ] Confirm that the email deployed successfully in the comments of the issue
* Close out this issue.

/label ~"ABM-Verticurl::blocked"
/confidential
