<!-- Purpose of this issue: To write copy for webcast. -->

## To Do
* [ ] Name this issue `Write Email Copy: <campaign name>` (ex. Write Email Copy: Security Webcast)
* [ ] Set issue due date based on [timeline guidelines](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)
* [ ] Clone this [template](https://docs.google.com/document/d/1ANOPTT8IwaVohsJQCiuLV4Ui2DSACQ3cYbz3Do6_Im0/edit?usp=sharing)
* [ ] Write copy for sales nominated invite in copy doc 
* [ ] Write copy for email Invite 1 in copy doc
* [ ] Write copy for email Invite 2 in copy doc 
* [ ] Write copy for email Reminder in copy doc 
* [ ] Write copy for email Follow Up in copy doc
* [ ] Ensure [Request Target List Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_target_list_creation) is submitted 

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"ABM Campaign"
/confidential
-->
