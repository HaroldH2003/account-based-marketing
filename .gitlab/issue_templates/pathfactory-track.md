This issue is to be used by ABM managers who will be creating a PathFactory track for an ABM campaign. More information on using PathFactory at GitLab can be found [here](/handbook/marketing/marketing-operations/pathfactory/). This issue is for PF tracks only. Please open this issue (LINK TO COME) if you are wanting to create/update an explore (landing) page. 

*Note: please title this issue [Campaign Name] - PathFactory Track

### [PathFactory Track](/handbook/marketing/marketing-operations/pathfactory/#content-tracks)
* [ ] Recommended track
* [ ] Target track

### Additional notes/needs

### :white_check_mark: Path link 
- Path to be linked here

### Track review:
Best practices for configuring a content track can be found [here](/handbook/marketing/marketing-operations/pathfactory/#configure-content-track-settings). If you are not able to complete the track review section, please open [this issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=pathfactory_qa_review) to have the MktgOps team complete the review. 
- [ ] All gated content has form strategy enabled
- [ ] All gated content has a listening campaign set up in Marketo
- [ ] If form strategy enabled, `Show to known users` is unchecked
- [ ] `Search Engine Directive` is set to `No Index, No Follow`
- [ ] Asset titles in the track have no `| GitLab` 
- [ ] `Cookie Consent` is turned on
- [ ] `External ID` is set to default

/label ~"ABM" ~"ABM-Priority::TBD" ~"mktg-status::plan"
