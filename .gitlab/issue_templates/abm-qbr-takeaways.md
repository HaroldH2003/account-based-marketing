## Setup
* Name this issue `QBR Takeaways: [Segment] [Region] (FY?Q?)` (ex. `QBR Takeaways: Enterprise EMEA (FY22Q3)`)
* Update milestone, due date, and assignee in project management section at bottom of issue

## Key Links
* 
* 

## Key Takeaways
<!-- Briefly outilne findings that are relevant to demand gen team, inluding digital and partner. Please place in order of priority. -->
1. 
1. 
1.

## Ideas / Optimization
<!-- Outline ideas that were sparked during the QBR, or discussed during the session. -->
* 
* 
* 

## Other Notes
<!-- Include any additional notes, for example, updates requested of the next iteration of data, or people to follow up with. -->
* 
* 
* 

<!-- Please leave the labels below on this issue -->
/label ~"mktg-status::wip" ~"ABM campaign" ~"ABM" ~"ABM-Priority::TBD"
/confidential
