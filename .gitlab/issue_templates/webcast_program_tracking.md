<!-- Purpose of this issue: For ABM Managers to create the Marketo program and SFDC campaign for a webcast. -->

## Webcast Program Tracking Action Item Checklist
* [ ] Name this issue `Program Tracking: [name of campaign]` (ex. Program Tracking: Modernize CI/CD Webcast)
* [ ] Set a due date for the issue based on the [SLA Workback Schedule](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] Set up [Zoom](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-1-configure-zoom) (this should be completed *before* setting up program tracking below)
* [ ] Create Marketo program - [process to follow](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#marketo-program-and-salesforce-campaign-set-up) and [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-2-set-up-the-webcast-in-marketosfdc-and-integrate-to-zoom)
* [ ] Create Salesforce campaign - [process to follow](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#marketo-program-and-salesforce-campaign-set-up)
* [ ] Add Marketo and SFDC links to epic

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"ABM Campaign"
/confidential
-->
