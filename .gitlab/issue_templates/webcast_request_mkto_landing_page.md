<!-- Purpose of this issue: To request a landing page be created in Marketo with relevant marketing automation. -->

## Submitter Checklist
* [ ] Name this issue `Marketo LP Copy and Automation: Webcast name` 
* [ ] Due date: Set due date based on [SLA workback timeline](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] Review [landing page best practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/landing-pages/#landing-page-conversion-best-practices)
* **Type of Marketo landing page** (select one)
   - [x] Buyer Progression Webcast
* [ ] Use the document from the `Write Copy` issue as your template
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable.
* [ ] Provide speaker image (200x200):
* [ ] Confirm the character limit template located in the copy doc was utilized for final copy
* [ ] Change due date to `-23 business days pre webcast` [(SLA workback timeline for assistance)](https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)
* [ ] Once this section is fully complete, change ~"ABM-Verticurl::blocked" label to ~"ABM-Verticurl::wip" and assign Verticurl Project Manager.

## Marketo work to be completed 
* [ ] Update Marketo tokens
* [ ] Update registration landing page URL
* [ ] Update thank you landing page URL (if applicable)
* [ ] Update the "resulting page" of the form (to make it the thank you page, if applicable)
* [ ] Activate smart campaign(s)
* [ ] Test live registration page and flows
* [ ] Add registration URL to epic
* Close out this issue.

**Need help getting started? [Learn more in the handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/landing-pages/#marketo-landing-pages-general)**


/label ~"ABM-Verticurl::blocked"
/confidential
