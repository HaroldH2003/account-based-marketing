## Campaign Overview


## Campaign Details
- [ ] Demandbase account list: 
- [ ] Exported account list for Growth Kitchen: 
- [ ] Added to [Marketing Calendar](https://docs.google.com/spreadsheets/d/1ni6gKeWhjtrNppMdYvPESsCRjDbfVdYjTNtUtcNBFGg/edit#gid=571560493)

## :calendar: Campaign flight dates  
- Start date:
- End date:

## Ads
- [ ] Canva folder: 

## Landing page:
- Landing page: 
- Landing page link with UTM added: 

## :dollar: Budget
- Budget for this campaign? `amount`










/label ~"Account Based Marketing" ~"mktg-status::plan" ~"ABM" ~"ABM-Priority::TBD" ~"growthkitchen"
/confidential
/milestone %triage-ab_strategy
/milestone %triage-ab_strategy
